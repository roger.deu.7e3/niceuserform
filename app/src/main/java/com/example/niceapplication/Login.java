package com.example.niceapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class Login extends AppCompatActivity {

    private TextInputLayout username, password;
    private TextInputEditText usernameText, passwordText;
    private MaterialButton login, register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = findViewById(R.id.textInputLayout);
        usernameText = findViewById(R.id.usernameText);

        password = findViewById(R.id.passwordTextLay);
        passwordText = findViewById(R.id.passwordText);

        login = findViewById(R.id.materialButton);
        register = findViewById(R.id.buttonRegister);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this,register.class);
                startActivity(i);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usernameText.getText().toString().isEmpty() && passwordText.getText().toString().isEmpty()) {
                    Toast.makeText(Login.this, "Error: fields are empty", Toast.LENGTH_SHORT).show();
                } else {
                    Intent i = new Intent(Login.this,Welcome.class);
                    startActivity(i);
                }
            }
        });
    }
}
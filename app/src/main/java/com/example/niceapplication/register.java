package com.example.niceapplication;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class register extends AppCompatActivity {

    private TextInputEditText username;
    private TextInputLayout usernameLayout;
    private TextInputEditText password;
    private TextInputLayout passwordLayout;
    private TextInputEditText email;
    private TextInputLayout emailLayout;
    private TextInputEditText repeat;
    private TextInputLayout repeatLayout;
    private TextInputEditText name;
    private TextInputLayout nameLayout;
    private TextInputEditText surname;
    private TextInputLayout surnameLayout;
    private TextInputEditText date;
    private TextInputLayout dateLayout;
    private MaterialAutoCompleteTextView gender;
    private TextInputLayout genderLayout;

    private MaterialButton login, register;

    private MaterialCheckBox checkBox;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        username = findViewById(R.id.textoNombreUsuario);
        usernameLayout = findViewById(R.id.usuarioLayout);
        password = findViewById(R.id.textPassword);
        passwordLayout = findViewById(R.id.passwordLayout);
        email = findViewById(R.id.textoMail);
        emailLayout = findViewById(R.id.emailLayout);
        repeat = findViewById(R.id.repeatPassword);
        repeatLayout = findViewById(R.id.repeatPasswordLay);
        name = findViewById(R.id.textoNombre);
        nameLayout = findViewById(R.id.nombreLay);
        surname = findViewById(R.id.textoApellidos);
        surnameLayout = findViewById(R.id.apellidosLay);
        date = findViewById(R.id.fechaNacimiento);
        dateLayout = findViewById(R.id.birthdateLay);
        gender = findViewById(R.id.genderPronoun);
        genderLayout = findViewById(R.id.genderLay);

        checkBox = findViewById(R.id.checkbox);
        login = findViewById(R.id.buttonLogin);
        register = findViewById(R.id.buttonRegister);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (username.getText().toString().isEmpty() || name.getText().toString().isEmpty() ||
                        surname.getText().toString().isEmpty() || email.getText().toString().isEmpty() ||
                        date.getText().toString().isEmpty() || gender.getText().toString().isEmpty() ||
                        password.getText().toString().isEmpty() || repeat.getText().toString().isEmpty() || !checkBox.isChecked()) {
                    Toast.makeText(register.this, "Falta algun campo", Toast.LENGTH_SHORT).show();
                    if (password.getText().toString().isEmpty()) {
                        passwordLayout.setError("required field");
                    }
                    if (username.getText().toString().isEmpty()) {
                        usernameLayout.setError("required field");
                    }
                    if (email.getText().toString().isEmpty()) {
                        emailLayout.setError("required field");
                    }
                } else {
                    if (password.getText().toString().equalsIgnoreCase(repeat.getText().toString())) {
                        Intent i = new Intent(register.this,Welcome.class);
                        startActivity(i);
                    } else {
                        Toast.makeText(register.this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    MaterialDatePicker.Builder<Long> materialDatePicker = MaterialDatePicker.Builder.datePicker();
                    materialDatePicker.setTitleText("Data");
                    final MaterialDatePicker<Long> picker = materialDatePicker.build();
                    picker.show(getSupportFragmentManager(), picker.toString());
                    if (picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener<Long>() {
                        @Override
                        public void onPositiveButtonClick(Long selection) {
                            date.setText(String.valueOf(picker.getHeaderText()));
                        }
                    })) ;
                }
                return true;
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(register.this,Login.class);
                startActivity(i);
            }
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.modules, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender.setAdapter(adapter);

        username.addTextChangedListener(new TextWatcher() {
             @Override
             public void beforeTextChanged(CharSequence s, int start, int count, int after) {

             }

             @Override
             public void onTextChanged(CharSequence s, int start, int before, int count) {
                 if (username.getText().toString().isEmpty()) {
                     usernameLayout.setError("required field");
                 } else {
                     usernameLayout.setError(null);
                 }
             }

             @Override
             public void afterTextChanged(Editable s) {

             }
         });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (password.getText().toString().isEmpty()) {
                    passwordLayout.setError("required field");
                } else {
                    passwordLayout.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (password.getText().toString().length() < 8) {
                    passwordLayout.setError("Error password must have at least 8 characters");
                } else {
                    passwordLayout.setError(null);
                }
            }
        });

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (email.getText().toString().isEmpty()) {
                    emailLayout.setError("required field");
                } else {
                    emailLayout.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}